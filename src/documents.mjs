import { KAREO_BASE_URL } from './helpers.mjs';

const KAREO_PATIENT_DOCUMENT_PAGE = `${KAREO_BASE_URL}/EhrWebApp/patients/viewDocuments`;
const DOCUMENTS_TABLE_PAGER = '#documents_pager #documents_pager_center';
const DOCUMENTS_TABLE_ROWS = '#documents-table tr';
const DOCUMENTS_TABLE_PAGE_NUMBER = `${DOCUMENTS_TABLE_PAGER} span#sp_1`;
const DOCUMENTS_TABLE_NEXT = `${DOCUMENTS_TABLE_PAGER} #next`;
const LOADER = '#load_documents-table';

export const navigate = async (page, patientId) =>
  await page.goto(`${KAREO_PATIENT_DOCUMENT_PAGE}/${patientId}`, {
    waitUntil: 'domcontentloaded',
  });

export const numberOfDocumentsPages = async (page) => {
  await page.waitForSelector(DOCUMENTS_TABLE_PAGE_NUMBER);
  const n = await page.$eval(DOCUMENTS_TABLE_PAGE_NUMBER, (el) =>
    el.innerText.trim()
  );
  return parseInt(n, 10);
};

export const nextPageOfDocuments = async (page) => {
  const nextButton = await page.$(DOCUMENTS_TABLE_NEXT);
  await Promise.all([waitForLoader(page), nextButton.click()]);
};

const waitForLoader = async (page) => {
  await page.waitForSelector(LOADER, { state: 'visible' });
  await page.waitForSelector(LOADER, { state: 'hidden' });
};

export async function* getCurrentTableRows(page, cache) {
  const rows = await page.$$(DOCUMENTS_TABLE_ROWS);

  for (const row of rows) {
    const date = await row.$eval('.dates', (el) => el.innerText.trim());
    const name = await row.$eval('.document-name', (el) => el.innerText.trim());
    const label = await row.$eval('.document-type', (el) =>
      el.innerText.trim()
    );
    const cache_id = `${date}_${name}_${label}`;

    if (cache.includes(cache_id)) {
      continue;
    }

    const downloadButton = await row.$('.document-edit > a:nth-child(2)');
    const [download] = await Promise.all([
      page.waitForEvent('download'),
      downloadButton.click(),
    ]);
    const path = await download.path();
    const filename = download.suggestedFilename().trim();

    cache.push(cache_id);
    yield { date, filename, label, path };
  }
}
