import { chromium } from 'playwright';
import yargs from 'yargs';
import { patientsCursor } from './patients.mjs';
import { newPatientTab } from './patient.mjs';
import { login } from './login.mjs';
import { newTabPool } from './tab_pool.mjs';

const argv = yargs(process.argv.slice(2))
  .usage(
    'Usage: $0 -u username -p password [--headless] [--slowmo <int>] [--tabs <int>]'
  )
  .alias('u', 'username')
  .describe('u', 'username to log in with')
  .alias('p', 'password')
  .describe('p', 'password to log in with')
  .boolean('headless')
  .describe('headless', 'run in headless mode')
  .default('headless', false)
  .number('slowmo')
  .describe('slowmo', 'slowMo value to pass to the playwright browser')
  .default('slowmo', 100)
  .number('tabs')
  .describe('tabs', 'number of tabs to use at one time')
  .default('tabs', 1)
  .help('h')
  .alias('h', 'help')
  .demandOption(['u', 'p']).argv;

(async () => {
  const { u: username, p: password, headless, slowmo, tabs } = argv;

  const browser = await chromium.launch({
    channel: 'chrome',
    headless,
    slowMo: slowmo,
  });
  const context = await browser.newContext({
    userAgent:
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
  });
  const mainPage = await context.newPage();
  await login(mainPage, username, password);

  const tabFactory = await newTabPool(context, tabs);
  await tabFactory.ready();

  for await (const { id, name } of patientsCursor(mainPage)) {
    const tab = await tabFactory.acquire();

    (async function (tabFactory, tab, id, name) {
      const p = await newPatientTab(tab, id, name);

      if (p.isComplete()) {
        await p.stopAndFlushData();
        await tabFactory.release(tab);
        return;
      }

      if (!p.areDocsComplete()) {
        for await (const file of p.documentsCursor()) {
        }
        await p.docsComplete();
      }

      if (!p.areNotesComplete()) {
        for await (const file of p.notesCursor()) {
        }
        await p.notesComplete();
      }

      console.log(`client ${name} complete\n`);
      await p.stopAndFlushData();
      await tabFactory.release(tab);
    })(tabFactory, tab, id, name);
  }

  await mainPage.close();
  await tabFactory.drain();
  await tabFactory.clear();
  await browser.close();
})();
