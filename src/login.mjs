import { clickAndWait, KAREO_BASE_URL } from './helpers.mjs';

const KAREO_LOGIN_PAGE = `${KAREO_BASE_URL}/login-ui/#/login`;
const LOGIN_FORM_PASSWORD = '#login_form #password';
const LOGIN_FORM_SUBMIT = '#login_form #sign-in';
const LOGIN_FORM_USERNAME = '#login_form #userName';

export const navigate = async (page) =>
  await page.goto(KAREO_LOGIN_PAGE, { waitUntil: 'domcontentloaded' });

export const login = async (page, username, password) => {
  await navigate(page);

  await page.fill(LOGIN_FORM_USERNAME, username);
  await page.fill(LOGIN_FORM_PASSWORD, password);

  const loginButton = await page.$(LOGIN_FORM_SUBMIT);
  await clickAndWait(page, loginButton);
};
