import { KAREO_BASE_URL } from './helpers.mjs';
import { setTimeout } from 'timers/promises';

const KAREO_PATIENTS_PAGE = `${KAREO_BASE_URL}/EhrWebApp/patients/list/`;
const PATIENTS_TABLE_PAGER = '#patients_pager #patients_pager_center';
const PATIENTS_TABLE_ROWS = '#patients tr';
const PATIENTS_TABLE_PAGE_NUMBER = `${PATIENTS_TABLE_PAGER} span#sp_1`;
const PATIENTS_TABLE_NEXT = `${PATIENTS_TABLE_PAGER} #next`;
const LOADER = '#load_patients';

export async function* patientsCursor(page) {
  await navigate(page);
  await setTimeout(2000);
  const totalPatientPages = await numberOfPatientPages(page);

  for (let i = 0; i < totalPatientPages; i++) {
    if (i > 0) {
      await nextPageOfPatients(page);
    }
    const patientsInfo = await patientsBasicInformation(page);
    for (const info of patientsInfo) {
      yield info;
    }
  }
}

const navigate = async (page) =>
  await page.goto(KAREO_PATIENTS_PAGE, { waitUntil: 'domcontentloaded' });

const nextPageOfPatients = async (page) => {
  const nextButton = await page.$(PATIENTS_TABLE_NEXT);
  await Promise.all([waitForLoader(page), nextButton.click()]);
};

const waitForLoader = async (page) => {
  await page.waitForSelector(LOADER, { state: 'visible' });
  await page.waitForSelector(LOADER, { state: 'hidden' });
};

const numberOfPatientPages = async (page) => {
  const n = await page.$eval(PATIENTS_TABLE_PAGE_NUMBER, (el) => el.innerText);
  return parseInt(n, 10);
};

const patientsBasicInformation = async (page) =>
  await page.$$eval(PATIENTS_TABLE_ROWS, (rows) =>
    rows.map((r) => ({
      id: r.children[0].innerText,
      name: r.children[1].innerText,
    }))
  );
