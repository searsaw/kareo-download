import genericPool from 'generic-pool';

export const newTabPool = async (context, tabs) => {
  const browser = context.browser();
  const cookies = await context.cookies();
  const factory = {
    // a new context is created instead of just adding a tab to the existing one
    // because some issues were found around pages navigating to the same url in
    // multiple tabs when part of the same context
    create: async function () {
      const newCtx = await browser.newContext({
        acceptDownloads: true,
        userAgent:
          'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
      });
      await newCtx.addCookies(cookies);
      return newCtx.newPage();
    },
    destroy: async function (tab) {
      const ctx = tab.context();

      await tab.close();
      await ctx.close();
    },
  };

  return genericPool.createPool(factory, { min: 2, max: tabs });
};
