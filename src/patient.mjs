//@ts-check
import { mkdir, writeFile, readFile, copyFile } from 'fs/promises';
import { join } from 'path';
//@ts-expect-error
import { directoryNameFromFullName } from './helpers.mjs';
//@ts-expect-error
import * as documents from './documents.mjs';
//@ts-expect-error
import * as notes from './notes.mjs';
import { parse as parseDate, format as formatDate } from 'date-fns';
import sanitizeFilename from 'sanitize-filename';
import { setTimeout } from 'timers/promises';
import playwright from 'playwright';

/**
 *
 * @param {playwright.Page} page
 * @param {string} id
 * @param {string} name
 */
export const newPatientTab = async function (page, id, name) {
  await ensureDirectories(name);

  const data = await getPatientData(name);
  const intervalId = setInterval(async () => {
    await savePatientData(data);
  }, 3000);
  const moveDocument = async (filename, tempLocation) => {
    await moveDownloadedFileToDirectory(
      data.name,
      tempLocation,
      'documents',
      filename
    );
  };
  const moveNote = async (filename, tempLocation) => {
    await moveDownloadedFileToDirectory(
      data.name,
      tempLocation,
      'notes',
      filename
    );
  };
  const isComplete = () => areDocsComplete() && areNotesComplete();
  const areDocsComplete = () => data.documents.complete;
  const areNotesComplete = () => data.notes.complete;
  const docsComplete = async () => {
    data.documents.complete = true;
    await savePatientData(data);
  };
  const notesComplete = async () => {
    data.notes.complete = true;
    await savePatientData(data);
  };
  const stopAndFlushData = async () => {
    clearInterval(intervalId);
    await savePatientData(data);
  };

  async function* documentsCursor() {
    await documents.navigate(page, id);
    await setTimeout(2000);
    const documentPages = await documents.numberOfDocumentsPages(page);
    // ensure there are pages
    if (documentPages === 0) {
      return;
    }

    for (let i = 0; i < documentPages; i++) {
      if (i > 0) {
        await documents.nextPageOfDocuments(page);
      }
      for await (const row of documents.getCurrentTableRows(
        page,
        data.documents.files
      )) {
        const date = parseDate(row.date, 'dd LLL yyyy', new Date());
        const filename = `${formatDate(date, 'yyyy-MM-dd')}_${row.label.replace(
          /[\s/]/g,
          '_'
        )}_${sanitizeFilename(row.filename)}`;
        try {
          await moveDocument(filename, row.path);
        } catch (e) {
          console.error(e);
          throw e;
        }

        yield filename;
      }
    }
  }

  async function* notesCursor() {
    await notes.navigate(page, id);
    await setTimeout(2000);
    const notePages = await notes.numberOfPages(page);
    // ensure there are pages. this only happens if the patient has zero notes.
    if (notePages === 0) {
      return;
    }

    for (let i = 0; i < notePages; i++) {
      for await (const row of notes.getCurrentTableRows(
        name,
        page,
        i + 1,
        data.notes.files
      )) {
        try {
          const date = parseDate(row.date, 'L/d/yy', new Date());
          const filename = `${formatDate(
            date,
            'yyyy-MM-dd'
          )}_${row.label.replace(/[\s/]/g, '_')}_${row.noteId}.pdf`;
          await moveNote(filename, row.path);
          yield filename;
        } catch (e) {
          console.error(
            `error writing note data for ${name} on page ${page.url()} with data ${row}`
          );
          console.error(e);
          throw e;
        }
      }
    }
  }

  return {
    id,
    name,
    documentsCursor,
    notesCursor,
    isComplete,
    docsComplete,
    notesComplete,
    areDocsComplete,
    areNotesComplete,
    stopAndFlushData,
  };
};

const patientDirectory = (name) =>
  join('.', 'patient_files', directoryNameFromFullName(name));

const ensureDirectories = async (name) =>
  Promise.all([
    mkdir(patientDirectory(name), { recursive: true }),
    mkdir(join(patientDirectory(name), 'documents'), { recursive: true }),
    mkdir(join(patientDirectory(name), 'notes'), { recursive: true }),
  ]);

const getPatientData = async (name) => {
  const metadataFile = join(patientDirectory(name), 'metadata.json');

  try {
    const contents = await readFile(metadataFile, { encoding: 'utf8' });
    return JSON.parse(contents);
  } catch (e) {
    return {
      name,
      documents: {
        complete: false,
        total: 0,
        files: [],
      },
      notes: {
        complete: false,
        total: 0,
        files: [],
      },
    };
  }
};

const savePatientData = async (data) => {
  const metadataFile = join(patientDirectory(data.name), 'metadata.json');
  data.documents.total = data.documents.files.length;
  data.notes.total = data.notes.files.length;

  try {
    await writeFile(metadataFile, JSON.stringify(data, null, 2));
  } catch (e) {
    console.error(
      `error saving file ${metadataFile}. will try again on next interval`
    );
  }
};

const moveDownloadedFileToDirectory = async (
  name,
  oldPath,
  directory,
  filename
) => await copyFile(oldPath, join(patientDirectory(name), directory, filename));
