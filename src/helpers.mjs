//@ts-check
export const KAREO_BASE_URL = 'https://app.kareo.com';

export const clickAndWait = (page, element) =>
  Promise.all([
    page.waitForNavigation({ waitUntil: 'load' }),
    element.click({ delay: 100 }),
  ]);

const titleCase = (str) =>
  str
    .toLowerCase()
    .split(' ')
    .filter((a) => !!a)
    .map((word) => word.replace(word[0], word[0].toUpperCase()))
    .join(' ');

export const directoryNameFromFullName = (name) =>
  titleCase(name.trim().replace(/'-/g, ' ').replace(',', '')).replace(
    /\s/g,
    '_'
  );
