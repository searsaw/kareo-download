# Kareo Documents/Notes Downloader

Kareo isn't great. There isn't a good way to quickly download all the documents and notes from all patients. To automate this, this script was born! It uses [Playwright](https://playwright.dev/) to spin up a headless browser that logs in as a specific user. It then navigates to the patients page and iterates through the list, spinning up new tabs to then download the documents and notes from each patient. The max number of tabs to use at one time is configurable to allow scaling depending on the machine the process is running on. It is not spun up in headless mode by default to enable debugging. However, it will need to be run headless to allow Playwright to download the note PDFs. PDF download using Playwright is only supported in headless Chromium.

## Running

To run the script, use `npm start` with the correct flags.

```shell
❯ npm start -- --help

> kareo_download@1.0.0 start
> node --inspect src/index.mjs "--help"

Debugger listening on ws://127.0.0.1:9229/f93cc34c-76df-40be-a3a4-8a5d833166ba
For help, see: https://nodejs.org/en/docs/inspector
Usage: index.mjs -u username -p password [--headless] [--slowmo <int>] [--tabs <
int>]

Options:
      --version   Show version number                                  [boolean]
  -u, --username  username to log in with                             [required]
  -p, --password  password to log in with                             [required]
      --headless  run in headless mode                [boolean] [default: false]
      --slowmo    slowMo value to pass to the playwright browser
                                                         [number] [default: 100]
      --tabs      number of tabs to use at one time        [number] [default: 1]
  -h, --help      Show help                                            [boolean]
```

A typical run would look like the following:

```shell
KAREO_USERNAME=<username>
KAREO_PASSWORD=<password>
npm start -- --username $KAREO_USERNAME --password $KAREO_PASSWORD --headless --tabs 10
```

**Note: There currently appears to be a memory leak somewhere. The author does not have time to search it out. Therefore, it has been run in the past like so.**

```shell
until npm start -- --username $KAREO_USERNAME --password $KAREO_PASSWORD --headless --tabs 10; do sleep 5; done
```

## Rerunning

Sometimes you will want to run the script at one point and then run it later to pick up notes and documents that have been added over time. Since the `metadata.json` file in each patient's directory tracks when the script has gotten everything for that patient *at that time* and checks this boolean on subsequent runs, getting this update data is as easy as setting these booleans to `false`. Below is a command to do this for every patient.

**Note: this uses [`jq`](https://stedolan.github.io/jq/) and [`sponge` from `moreutils`](https://manpages.debian.org/jessie/moreutils/sponge.1.en.html)**

```shell
for file in patient_files/*/metadata.json; do
  cat "$file" \
    | jq '.documents.complete = false | .notes.complete = false' \
    | sponge "$file"
done
```
